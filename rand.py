import random
from zero_player import get_graph_info
from utils.run import NUM_ROUNDS

def random_nodes(graph, k):
    nodes = graph.keys()
    return random.sample(nodes, k)

if __name__ == "__main__":
    graph, num_players, num_seeds = get_graph_info()
    seeds = random_nodes(graph, num_seeds)
    for i in xrange(NUM_ROUNDS):
        for s in seeds:
            print s
