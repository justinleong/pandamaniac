from zero_player import get_graph_info
from utils.run import NUM_ROUNDS
#from highest_degree import f
from eigenvector import f
import numpy as np
import networkx as nx


if __name__ == "__main__":
    data, num_players, num_seeds = get_graph_info()
    nodes = f(data, num_players, num_seeds)
    G = nx.from_dict_of_lists(data)
    if num_players == 27:
        num_players = 16
    mean = num_players*num_seeds/8
    scale = num_players*num_seeds/6 
    # print "Mean " + str(mean)
    # print "Scale " + str(scale)
    # print "Number of nodes " + str(len(nodes))
    for i in xrange(NUM_ROUNDS):
        rand_nodes = []
        num_remove = 35
        while len(rand_nodes) < num_seeds + num_remove:
            index = np.random.normal(mean, scale)
            index = int(index)
            if index >= 0 and index < num_players*num_seeds:
                if nodes[index] not in rand_nodes:
                    rand_nodes.append(nodes[index])
                    # print "Chose node with index {0}".format(index)
        remove_dict = nx.degree_centrality(G)
        remove_list = sorted(remove_dict, key=remove_dict.get, reverse=True)
        for j in xrange(num_remove):
            if remove_list[j] in rand_nodes:
                rand_nodes.remove(remove_list[j])
        for j in xrange(num_seeds):
            print rand_nodes[j]
