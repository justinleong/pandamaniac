from utils.run import NUM_ROUNDS
from zero_player import get_graph_info
import networkx as nx
from kobe_stopper import prune_graph
import json
import os

BETWEEN = "betweenness.py"
DEGREE = "highest_degree.py"
DEGREE_STOPPER = "degree_stopper.py"
KOBE_STOPPER = "kobe_stopper.py"
CLOSE = "closeness.py"
EIGEN = "eigenvector.py"
HILL = "hill_climb.py"
DISCOUNT = "degree_discount.py"
RANDOM = "rand.py"

PLAYER = EIGEN

def to_json(G):
    nodes = {}
    for node in G.nodes():
       nodes[node] = G.neighbors(node)
    return nodes


if __name__ == "__main__":
    graph, num_players, num_seeds = get_graph_info()
    g = prune_graph(graph, num_players, num_seeds)
    filename = "{0}.{1}.prune.json".format(num_players, num_seeds)
    f = open("Graphs/" + filename, "w")
    json.dump(to_json(g), f)
    f.close()
    os.system("python {0} {1}".format(PLAYER, filename))
