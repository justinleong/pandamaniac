import json
import random
from decimal import Decimal
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate gnp graphs.")
    parser.add_argument("-n", default=1000, type=int, help="Number of nodes")
    parser.add_argument("-p", default=0.1, help="Probability of edge")
    parser.add_argument("-s", default=100, help="Random seed value")
    
    args = parser.parse_args()

    random.seed(args.s)

    print "Generating graph with N = {0}, P = {1}".format(args.n, args.p)
    print "Seed is {0}".format(args.s)

    graph = {}
    for i in xrange(args.n): 
        graph[str(i)] = []
        for j in xrange(args.n):
            if j != i and random.random() < args.p: 
                graph[str(i)].append(str(j))

    print "Saving graph..."
    # Name file in the format gnp1000_01.json for N = 1000, P = 0.1
    f = open("Graphs/gnp{0}_{1}.json".format(args.n, 
                str(args.p).replace(".", "") ), "w")
    f.write(json.dumps(graph)) 
    f.close()
