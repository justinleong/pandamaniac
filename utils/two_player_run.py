import argparse
import os
import json
import sim

NUM_ROUNDS = 5
NUM_LEGIT_PLAYERS = 6

# Test player that does nothing
ZERO_PLAYER = "zero_player.py"

# Name players with constants
BETWEEN = "betweenness.py"
DEGREE = "highest_degree.py"
CLOSE = "closeness.py"
EIGEN = "eigenvector.py"
#HILL = "hill_climb.py"
HILL2 = "hill_climb2.py"
DEGREE_STOPPER = "degree_stopper.py"
KOBE_STOPPER = "kobe_stopper.py"
overall_wins = {}
overall_scores = {}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run simulation on a given file ")
    parser.add_argument("-f", default="2.5.1.json") 

    args = parser.parse_args()

    num_players, num_seeds, gid, ext = args.f.split(".")
    num_players = int(num_players)
    num_seeds = int(num_seeds)
    num_players = 2

    file_name = "Graphs/{0}".format(args.f)
    print "Running on {}".format(file_name)

    # Determines which strategy each player uses
    # max number of players is 8.
    players = [ EIGEN, DEGREE, DEGREE_STOPPER, KOBE_STOPPER,
                BETWEEN, CLOSE, KOBE_STOPPER, ZERO_PLAYER ]

    # Clean up from last time
    os.system("rm run/*.run")

    # Set up NUM_ROUNDS of competition.
    game_rounds = []
    for i in xrange(NUM_LEGIT_PLAYERS):
        os.system("python {0} {1} > run/{2}.run".format(players[i], args.f, i))
    k = 0
    overall_scores[players[NUM_LEGIT_PLAYERS-1]] = 0
    overall_wins[players[NUM_LEGIT_PLAYERS-1]] = 0
    # Try every combination of players
    for i in xrange(NUM_LEGIT_PLAYERS-1): 
        overall_scores[players[i]] = 0
        overall_wins[players[i]] = 0
        for j in xrange(NUM_LEGIT_PLAYERS-1-i):
            game_rounds.append({})
            f0 = open("run/{0}.run".format(i))
            f1 = open("run/{0}.run".format(NUM_LEGIT_PLAYERS-j-1))
            game_rounds[k][players[i]] = []
            game_rounds[k][players[NUM_LEGIT_PLAYERS-j-1]] = []
            rank = 0
            for l in f0:
                if rank == num_seeds:
                    break
                game_rounds[k][players[i]].append(l.strip())
                rank += 1
            rank = 0
            for l in f1:
                if rank == num_seeds:
                    break
                game_rounds[k][players[NUM_LEGIT_PLAYERS-j-1]].append(l.strip())
                rank += 1
            k = k + 1
            f0.close()
            f1.close()
    raw_file = open(file_name)
    graph = json.loads(raw_file.read())
    raw_file.close() 
    k = 0 
    # Run each round and print results
    for i in xrange(NUM_LEGIT_PLAYERS-1):
        for j in xrange(NUM_LEGIT_PLAYERS-1-i):
            results = sim.run(graph, game_rounds[k])
            print "Round {0} results: {1}".format(k,results)
            sorted_results = sorted(results, key=results.get, reverse=True)
            k = k + 1
            first = 1
            for l in xrange(num_players):
                if first == 1:
                    first = 0
                    overall_wins[sorted_results[l]] += 1
                    # Add point for tie
                    if results[sorted_results[l]] == results[sorted_results[1]]:
                        overall_wins[sorted_results[1]] += 1
                print "Strategy: {}.run result: {}".format(sorted_results[l],
                        results[sorted_results[l]])
                overall_scores[sorted_results[l]] += results[sorted_results[l]]
    # Sort victors
    sorted_scores = sorted(overall_scores, key=overall_scores.get, reverse=True)
    sorted_wins =  sorted(overall_wins, key = overall_wins.get, reverse=True)
    # Print sorted and raw results
    print overall_scores
    print overall_wins
    print sorted_scores
    print sorted_wins
