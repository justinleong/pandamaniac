import argparse
import os
import json
import sim

NUM_ROUNDS = 50

# Test player that does nothing
ZERO_PLAYER = "zero_player.py"
TA_more = False 

# Name players with constants
BETWEEN = "betweenness.py"
DEGREE = "highest_degree.py"
DEGREE_STOPPER = "degree_stopper.py"
KOBE_STOPPER = "kobe_stopper.py"
SHAQ = "the_brick_wall.py"
CLOSE = "closeness.py"
EIGEN = "eigenvector.py"
CLUSTER = "clustering.py"
HILL = "hill_climb.py"
HILL2 = "hill_climb2.py"
HILL4 = "hill_climb4.py"
DISCOUNT = "degree_discount.py"
RANDOM = "rand.py"
LAKERS = "lakers.py"
RAND_BEST = "rand_optimal.py"
RAND_DEGREE = "rand_optimal_degree.py"
RAND_CLOSE = "rand_optimal_closeness.py"
RAND_CLUSTER = "rand_optimal_cluster.py"
RAND_GREEDY = "rand_greedy.py"
RAND_GREEDY_DEGREE = "rand_greedy_degree.py"
PSEUDO = "pseudo_cluster.py"
VISUAL = "degree_counter.py"
BROWN = "4_brown.py"
CLUSTERPHOBES = "clusterphobes.py"
HORSE = "horse_with_no_name.py"
BOB = "team_bob.py"
YKST = "ykst.py"
KCSS = "kcss.py"

# List teams to remove from downloaded file
REMOVE = ["MidnightBedlamites", "nana"]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run simulation on a given file ")
    parser.add_argument("-f", help="graph (ex: 8.20.1.json)") 
    parser.add_argument("-d", help="downloaded results file") 

    args = parser.parse_args()

    if args.f != None:
        num_players, num_seeds, gid, ext = args.f.split(".")
        num_players = int(num_players)
        num_seeds = int(num_seeds)
        new_f = str(num_players) + "." + str(num_seeds+2) + "." + gid + "." + ext

    file_name = "Graphs/{0}".format(args.f)
    print "Running on {}".format(file_name)

    # Determines which strategy each player uses
    # max number of players is 8.
    total_teams = 0

    # Clean up from last time
    os.system("rm run/*.run")
    os.system("cp run/conglomerate27.txt run/15.run")

    # Set up NUM_ROUNDS of competition.
    game_rounds = []
    for i in xrange(NUM_ROUNDS): 
        game_rounds.append({})

    # RUN SIM OPTION 1: use file downloaded from website
    if args.d != None:
        download = args.d

        num_players, num_seeds, gid, ext = args.f.split(".")
        num_players = int(num_players)
        num_seeds = int(num_seeds)

        # Load seed nodes for each round into game_rounds
        with open(download, 'r') as f:
            data = f.readlines() # one line
            # BE CAREFUL ABOUT WHAT YOU FEED TO EVAL()
            team_strategies = eval(data[0])

            # Remove REMOVE teams
            for rteam in REMOVE:
                team_strategies.pop(rteam, None) # return None if not found

            for i in xrange(NUM_ROUNDS):
                team_num = 0
                for team in team_strategies:
                    game_rounds[i]["{0} {1}d".format(team, team_num)] = team_strategies[team][i][:num_seeds]
                    team_num += 1
            total_teams = team_num

            # Write team_num.run files; 50 lines per file
            team_num = 0
            for team in team_strategies:
                with open("run/{}d.run".format(team_num), 'w') as f:
                    for rnd in team_strategies[team]:
                        rank = 0
                        for seed_node in rnd:
                            if rank == num_seeds:
                                rank = 0
                                break
                            f.write("{}\n".format(seed_node))
                            rank += 1
                team_num += 1


    # RUN SIM OPTION 2: STRATEGIES
    if args.f != None:
        new_players = num_players - total_teams
        # Determines which strategy each player uses
        # max number of players is 8.
        players = [ RAND_GREEDY, RAND_DEGREE, BROWN, CLUSTERPHOBES,
                    RAND_BEST, DEGREE, YKST, KCSS,
                    BOB, HORSE, RAND_GREEDY_DEGREE, RANDOM,
                    RANDOM, RANDOM, RANDOM, RANDOM,
                    ZERO_PLAYER, ZERO_PLAYER, ZERO_PLAYER, ZERO_PLAYER,
                    ZERO_PLAYER, ZERO_PLAYER, ZERO_PLAYER, ZERO_PLAYER,
                    ZERO_PLAYER, ZERO_PLAYER, ZERO_PLAYER, ZERO_PLAYER,
                    ZERO_PLAYER, ZERO_PLAYER, ZERO_PLAYER, ZERO_PLAYER] 
        # Set up NUM_ROUNDS of competition.
        for i in xrange(NUM_ROUNDS): 
            for j in xrange(new_players):
                game_rounds[i]["{0} {1}".format(players[j], j)] = []

        for i in xrange(new_players):
            if i != 16:
                os.system("python {0} {1} > run/{2}.run".format(players[i], args.f, i))
            if TA_more and i == 1:
                os.system("python {0} {1} > run/{2}.run".format(players[i], new_f, i))
            f = open("run/{0}.run".format(i))

            rank = 0
            if TA_more and i == 1:
                rank = -2
            cur_round = 0
            for line in f: 
                # print "cur_round {} rank {} ".format(cur_round, rank)
                game_rounds[cur_round]["{0} {1}".format(players[i], i)].append(line.strip())
                rank += 1 
                if rank == num_seeds:
                    rank = 0
                    if TA_more and i == 1:
                        rank = -2
                    cur_round += 1

            f.close()

    # Load graph
    raw_file = open(file_name)
    graph = json.loads(raw_file.read())
    raw_file.close() 

    # This line runs the simulation one time
    # results = sim.run(graph, game_rounds[0])
    wins = {}
    second = {}
    third = {}
    fourth = {}
    fifth = {}
    nodes_won = {}
    oldPlayers = players
    if args.d != None:
        players = []
        num_players = len(team_strategies)
        for team in team_strategies:
            players.append(team)
        for i in range(0, new_players):
            ALGORITHM = oldPlayers[i] + " " + str(i)
            wins[ALGORITHM] = 0
            second[ALGORITHM] = 0
            third[ALGORITHM] = 0
            fourth[ALGORITHM] = 0
            fifth[ALGORITHM] = 0
            nodes_won[ALGORITHM] = 0
            
    for i in range(0, len(players)):
        ALGORITHM = players[i] + " " + str(i)
        if args.d != None:
            ALGORITHM += "d"
        wins[ALGORITHM] = 0
        second[ALGORITHM] = 0
        third[ALGORITHM] = 0
        fourth[ALGORITHM] = 0
        fifth[ALGORITHM] = 0
        nodes_won[ALGORITHM] = 0
    last_minute = open("run/conglomerate_winners.run", "w")
    for i in xrange(NUM_ROUNDS):
        # This line runs it multiple times
        results = sim.run(graph, game_rounds[i])

        print "Round {0} results: {1}".format(i, results)
        sorted_results = sorted(results, key=results.get, reverse=True)
        print sorted_results
        
        final_file = open("run/output.run", "w")
        seeds = 0

        # Write winning seeds to a separate file
        winner_num = sorted_results[0].split(" ")[1]
        for wassup in game_rounds[i][sorted_results[0]]:
            last_minute.write(wassup + "\n")
        # Count who won which place and print results of round
        if args.d != None:
            for i in range(0, new_players):
                ALGORITHM = oldPlayers[i] + " " + str(i)
                if sorted_results[0] == ALGORITHM:
                    wins[ALGORITHM] += 1
                if sorted_results[1] == ALGORITHM:
                    second[ALGORITHM] += 1
                if sorted_results[2] == ALGORITHM:
                    third[ALGORITHM] += 1
                if sorted_results[3] == ALGORITHM:
                    fourth[ALGORITHM] += 1
                if sorted_results[4] == ALGORITHM:
                    fifth[ALGORITHM] += 1
            for j in xrange(new_players):
                ALGORITHM = oldPlayers[j] + " " + str(j)
                if sorted_results[j] != ALGORITHM:
                    nodes_won[ALGORITHM] += results[sorted_results[j]]
                print "Strategy: {}.run result: {}".format(sorted_results[j], 
                        results[sorted_results[j]])
                run_num = sorted_results[j].split(" ")[1]
                run_file = "run/{}.run".format(run_num)

                # If file is not empty (ZERO_PLAYER)
                """if os.stat(run_file).st_size != 0:
                    f = open(run_file) 
                    for k in xrange(int(num_seeds / 2.5)):
                        final_file.write(f.readline())
                        seeds += 1
                        if seeds == num_seeds: 
                            #print "{}".format(num_seeds)
                            break;
                    f.close()"""

                final_file.close()

        for i in range(0, len(players)):
            ALGORITHM = players[i] + " " + str(i)
            if args.d != None:
                ALGORITHM += "d"
            if sorted_results[0] == ALGORITHM:
                wins[ALGORITHM] += 1
            if sorted_results[1] == ALGORITHM:
                second[ALGORITHM] += 1
            if sorted_results[2] == ALGORITHM:
                third[ALGORITHM] += 1
            if sorted_results[3] == ALGORITHM:
                fourth[ALGORITHM] += 1
            if sorted_results[4] == ALGORITHM:
                fifth[ALGORITHM] += 1
        for j in xrange(num_players):
            try:
                ALGORITHM = players[j] + " " + str(j)
                if args.d != None:
                    ALGORITHM += "d"
                if sorted_results[j+new_players] != ALGORITHM:
                    nodes_won[ALGORITHM] += results[sorted_results[j+new_players]]
                print "Strategy: {}.run result: {}".format(sorted_results[j+new_players], 
                        results[sorted_results[j+new_players]])
                run_num = sorted_results[j+new_players].split(" ")[1]
                run_file = "run/{}.run".format(run_num)
            except IndexError:
                pass

            # If file is not empty (ZERO_PLAYER)
            """if os.stat(run_file).st_size != 0:
                f = open(run_file) 
                for k in xrange(int(num_seeds / 2.5)):
                    final_file.write(f.readline())
                    seeds += 1
                    if seeds == num_seeds: 
                        #print "{}".format(num_seeds)
                        break;
                f.close()"""

            final_file.close()

    # Print final results
    if args.d != None:
        for i in range(0, new_players):
            ALGORITHM = oldPlayers[i] + " " + str(i)
            print "------------------------------"
            print ALGORITHM
            print "First: " + str(wins[ALGORITHM])
            print "Second: " + str(second[ALGORITHM])
            print "Third: " + str(third[ALGORITHM])
            print "Fourth: " + str(fourth[ALGORITHM])
            print "Fifth: " + str(fifth[ALGORITHM])
            print "Nodes won: " + str(nodes_won[ALGORITHM])
            print "TOTAL: " + str(20 * int(wins[ALGORITHM]) + 15 * int(second[ALGORITHM]) + 12 * int(third[ALGORITHM]) + 9 * int(fourth[ALGORITHM]) + 6 * int(fifth[ALGORITHM]))

    for i in range(0, len(players)):
        ALGORITHM = players[i] + " " + str(i)
        if args.d != None:
            ALGORITHM += "d"
        print "------------------------------"
        print ALGORITHM
        print "First: " + str(wins[ALGORITHM])
        print "Second: " + str(second[ALGORITHM])
        print "Third: " + str(third[ALGORITHM])
        print "Fourth: " + str(fourth[ALGORITHM])
        print "Fifth: " + str(fifth[ALGORITHM])
        print "Nodes won: " + str(nodes_won[ALGORITHM])
        print "TOTAL: " + str(20 * int(wins[ALGORITHM]) + 15 * int(second[ALGORITHM]) + 12 * int(third[ALGORITHM]) + 9 * int(fourth[ALGORITHM]) + 6 * int(fifth[ALGORITHM]))

        
