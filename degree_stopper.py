import json
from zero_player import get_graph_info
from utils.run import NUM_ROUNDS
import networkx as nx

if __name__ == "__main__":
    data, num_players, num_seeds = get_graph_info()
    G = nx.from_dict_of_lists(data)
    # Inditialize degree counts to zero
    degree_count = {}
    for node in data:
        degree_count[node] = 0
    # Count degree of each node
    for node in data:
        for neighbor in data[node]:
            degree_count[neighbor] += 1
    
    # Print in order by degree
    sorted_list = sorted(degree_count, key=degree_count.get, reverse=True)
    # Choose top nodes and neighbors
    for i in xrange(NUM_ROUNDS):
        printed = []
        num_in_cluster = 5
        for k in xrange((int)(num_seeds/num_in_cluster)):
            l = 0
            while sorted_list[k+l] in printed:
                l += 1
            print sorted_list[k+l]
            printed.append(sorted_list[k+l])
            num_neighbors = degree_count[sorted_list[k+l]]
            neighbor_list = {}
            for neighbor in data[sorted_list[k+l]]:
                neighbor_list[neighbor] = G.degree(neighbor)
            for j in xrange(0):
                neighbor_list.pop(sorted_list[j], None)
            for used_nodes in printed:
                neighbor_list.pop(used_nodes, None)
            sorted_neighbors = sorted(neighbor_list, key=neighbor_list.get, reverse=True)
            for j in xrange(num_in_cluster-1):
                print sorted_neighbors[j]
                printed.append(sorted_neighbors[j])
        l = 0
        num_extra = num_seeds % num_in_cluster
        for j in xrange(num_extra):
            while sorted_list[k+l] in printed:
                l += 1
            print sorted_list[k+l]
