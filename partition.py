__docformat__ = "reconstructedtext en"

import networkx as nx
import random
from copy import deepcopy
from zero_player import get_graph_info

def spectral_partition(G,n1=None):
    """Return a partition of the network based on the
    second Fieldler Vector

    Parameters
    ----------
    G : NetworkX Graph
    n1 : int
      Size of one of the partitions

    Returns
    --------
    C : list of sets
      Partition of the Network

    Raises
    ------
    ImportError
      If NumPy is not available
    NetworkXError
      If n1 is not in [0,G.order()]"""
    try:
        import numpy as np
    except:
        raise ImportError("spectral_partition() \
                           requires NumPy: http://scipy.org/")
    if n1 is None:
        n1 = G.order()/2
    else:
        if not 0 <= n1 <= G.order():
            raise nx.NetworkXError("n1 must be 0 <= n1 <= G.order()")
        
    eig_vals,eig_vecs = np.linalg.eig(nx.laplacian(G))
    
    min_val = float('infinity')
    min_vec = None
    sec_min_val = float('infinity')
    sec_min_vec = None
    i=0
    for e_val in eig_vals:
        e_vec = eig_vecs[:,i]
        if e_val < min_val:
            sec_min_val = min_val
            sec_min_vec = min_vec
            min_val = e_val
            min_vec = e_vec
        elif min_val < e_val < sec_min_val:
            sec_min_val = e_val
            sec_min_vec = e_vec
        i+=1

    part_vec = sorted(zip(sec_min_vec,G.nodes()))
    
    C_small = [set(),set()]
    C_large = [set(),set()]

    for (v,n) in part_vec[:n1]:
        C_small[0].add(n)

    for (v,n) in part_vec[n1:]:
        C_small[1].add(n)

    for (v,n) in part_vec[-n1:]:
        C_large[0].add(n)

    for (v,n) in part_vec[:-n1]:
        C_large[1].add(n)

    cut_small = nx.cut_size(G,C_small,0,1)
    cut_large = nx.cut_size(G,C_large,0,1)

    if cut_small < cut_large:
        return C_small
    else:
        return C_large

def kernighan_lin(G,C_init=None,max_iter=100):
    """Partition Network using the Kernighan-Lin Algorithm

    This algorithm paritions a network into two communities
    by iteratively swapping the nodes that will reduce the
    cut between the two nodes the most.

    Parameters
    ----------
    G : NetworkX Graph
    C_init : list of sets option default (None)
      Community Partition
    max_iter : int
      Maximum number of times to attempt swaps
      to find an improvemement before giving up

    Returns
    -------
    C : list of sets
      Partition of the nodes of the graph

    Raises
    -------
    NetworkXError
      if C_init is not a valid partition of the
      graph into two communities"""
    if C_init is None:
        m1 = G.order()/2
        m2 = G.order()-m1
        C = nx.random_partition(G.nodes(),partition_sizes=[m1,m2])
    else:
        if not nx.is_partition(G,C_init):
            raise nx.NetworkXError("C_init doesn't partition G")
        if not len(C_init) == 2:
            raise nx.NetworkXError("C_init doesn't partition G into 2 communities")
        C = deepcopy(C_init)
            

    C_cut_cost = nx.cut_size(G,C,0,1)
    Cmin = deepcopy(C)
    Cnext = deepcopy(C)

    Cmin_cut_cost = C_cut_cost
    Cnext_cut_cost = C_cut_cost

    itrs = 0
    
    while Cmin_cut_cost <= C_cut_cost and itrs < max_iter:
        C = deepcopy(Cmin)
        C_cut_cost = Cmin_cut_cost
        Cnext = deepcopy(Cmin)
        Cnext_cut_cost = Cmin_cut_cost
        
        comm_0 = deepcopy(Cnext[0])
        comm_1 = deepcopy(Cnext[1])

        while comm_0 and comm_1:
            min_swap = G.number_of_edges()
            min_pair = (None,None)
            swap_change = {}
            for n in G.nodes_iter():
                n_comm = nx.affiliation(n,C)[0]
                swap_change[n] = len(set(G.neighbors(n)).\
                                     intersection(Cnext[n_comm])) - \
                                 len(set(G.neighbors(n)).\
                                     intersection(Cnext[1-n_comm]))
            for i in comm_0:
                for j in comm_1:
                    if G.has_edge(i,j):
                        swap_ij = swap_change[i] + swap_change[j] + 2
                    else:
                        swap_ij = swap_change[i] + swap_change[j]
                    if swap_ij < min_swap:
                        min_swap = swap_ij
                        min_pair = (i,j)
            Cnext[0].remove(min_pair[0])
            Cnext[1].remove(min_pair[1])
            Cnext[1].add(min_pair[0])
            Cnext[0].add(min_pair[1])
            Cnext_cut_cost += min_swap
            comm_0.remove(min_pair[0])
            comm_1.remove(min_pair[1])

            if Cnext_cut_cost < Cmin_cut_cost:
                Cmin = deepcopy(Cnext)
                Cmin_cut_cost = Cnext_cut_cost
        itrs += 1
    return C
   
if __name__ == "__main__":
    graph, num_players, num_seeds = get_graph_info()
    G = nx.from_dict_of_lists(graph)
    print spectral_partition(G)
