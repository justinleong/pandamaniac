# Calculate betweenness centrality based on Brande's algorithm
# http://algo.uni-konstanz.de/publications/b-fabc-01.pdf
import json
from collections import deque

# Returns the top nodes based on Betweenness centrality

graph = open('Graphs/gnp1000_01.json').read()
data = json.loads(graph)
# Inditialize dictionary of lists
P = {}
sigma = {}
d = {}
delta = {}
CB = {}
for v in data:
    CB[v]  = 0
for s in data:
    # Use S as a stack
    S = []
    for w in data:
        P[w] = []
        sigma[w] = 0
        d[w] = -1
        delta[w] = 0
    sigma[s] = 1
    d[s] = 0
    Q = deque()
    Q.append(s)
    while len(Q) > 0:
        v = Q.popleft()
        S.append(v)
        for w in data[v]:
            # w found for the first time?
            if d[w] < 0:
                Q.append(w)
                d[w] = d[v] + 1
            # shortest path to w via v?
            if d[w] == d[v]+1:
                sigma[w] = sigma[w] + sigma[v]
                P[w].append(v)
    # S returns vertices in order of non-increasing distance from s
    while len(S) > 0:
        w = S.pop()
        for v in P[w]:
            delta[v] = delta[v] + (sigma[v]/sigma[w])*(1 + delta[w])
            if w != s:
                CB[w] = CB[w] + delta[w];
for w in sorted(CB, key=CB.get, reverse=True):
    print w, CB[w]
