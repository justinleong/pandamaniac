import json
import hill_climb as hc

def load_json(filename):
	graph = open(filename).read()
	data = json.loads(graph)
	return data

def print_nodes(lst, filename):
	with open(filename, 'w') as f:
		for item in lst:
			f.write(str(item) + "\n")

if __name__ == '__main__':
	graph = load_json('Graphs/fivespokes.json')
	seeds = hc.run(graph, 1)
	print "seeds: ", seeds
	# print_nodes(seeds, "test.txt")
