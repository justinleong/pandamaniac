from zero_player import get_graph_info
from utils.run import NUM_ROUNDS
#from highest_degree import f
from eigenvector import f
import numpy as np
import random
import networkx as nx


if __name__ == "__main__":
    data, num_players, num_seeds = get_graph_info()
    nodes = f(data, num_players, num_seeds)
    G = nx.from_dict_of_lists(data)
    if num_players == 27:
        num_players = 16
    mean = num_players*num_seeds/2
    scale = num_players*num_seeds/6 
    # print "Mean " + str(mean)
    # print "Scale " + str(scale)
    # print "Number of nodes " + str(len(nodes))
    remove_dict = nx.degree_centrality(G)
    remove_list = sorted(remove_dict, key=remove_dict.get, reverse=True)
    degree_top = []
    for i in xrange(50):
        degree_top.append(remove_list[i])
    for i in xrange(NUM_ROUNDS):
        rand_nodes = []
        index = 0
        while len(rand_nodes) < num_seeds:
            if index >= 0 and index < 100:
                if nodes[index] not in rand_nodes and nodes[index] in degree_top:
                    rand_nodes.append(nodes[index])
                    # print "Chose node with index {0}".format(index)
            index += 1
        for j in xrange(num_seeds):
            print rand_nodes[j]
