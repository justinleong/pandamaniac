from netx import do_networkx
from netx import list_networkx
import networkx as nx

def f(a, b, c):
	""" Returns list of top nodes sorted by closeness"""
	return list_networkx(nx.closeness_centrality)

if __name__ == "__main__": 
    do_networkx(nx.closeness_centrality)
