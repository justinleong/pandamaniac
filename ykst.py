from zero_player import get_graph_info
from utils.run import NUM_ROUNDS
from highest_degree import f
#from eigenvector import f
import numpy as np
import random

# This is an approximation of the strategy used by YKST

if __name__ == "__main__":
    data, num_players, num_seeds = get_graph_info()
    nodes = f(data, num_players, num_seeds)
    if num_players == 27:
        num_players = 16
    mean = num_players*num_seeds/2
    scale = num_players*num_seeds/6 
    # print "Mean " + str(mean)
    # print "Scale " + str(scale)
    # print "Number of nodes " + str(len(nodes))
    for i in xrange(NUM_ROUNDS):
        rand_nodes = []
        while len(rand_nodes) < num_seeds:
            index = random.randint(0, 50)
            index = int(index)
            if index >= 0 and index < 70:
                if nodes[index] not in rand_nodes:
                    rand_nodes.append(nodes[index])
                    # print "Chose node with index {0}".format(index)
        for j in xrange(num_seeds):
            print rand_nodes[j]
