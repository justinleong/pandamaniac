import json
from zero_player import get_graph_info
from utils.run import NUM_ROUNDS
from pqdict import pqdict

if __name__ == "__main__":
    data, num_players, num_seeds = get_graph_info()
    degrees = pqdict(reverse=True)

    # Inditialize degree counts to zero
    degree_count = {}
    for node in data:
        degree_count[node] = 0
    # Count degree of each node
    for node in data:
        for neighbor in data[node]:
            degree_count[neighbor] += 1
    for node in data:
        degrees[node] = degree_count[node]
    
    # Print in order by degree
    sorted_list = []
    for i in xrange(num_seeds):
        node = degrees.pop()
        sorted_list.append(node)
        for neighbor in data[node]:
            if neighbor in degrees:
                degrees[neighbor] -= 1

    for i in xrange(NUM_ROUNDS):
        for j in xrange(num_seeds):
            print sorted_list[j] #, degree_count[sorted_list[j]]
