from netx import do_networkx
from netx import list_networkx
import networkx as nx

def f(a, b, c):
	""" Returns list of top nodes sorted by eigenvector"""
	return list_networkx(nx.eigenvector_centrality)

def g(data, num_players, num_seeds):
    G = nx.from_dict_of_lists(data)
    central_dict = nx.eigenvector_centrality(G)
    sorted_list = sorted(central_dict, key=central_dict.get, reverse=True) 
    return sorted_list

if __name__ == "__main__":
    do_networkx(nx.eigenvector_centrality)
