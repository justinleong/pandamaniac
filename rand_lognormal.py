from zero_player import get_graph_info
from utils.run import NUM_ROUNDS
from highest_degree import f
# from eigenvector import f
import numpy as np


if __name__ == "__main__":
    data, num_players, num_seeds = get_graph_info()
    nodes = f(data, num_players, num_seeds)
    mean = 0.05 * len(nodes)
    mean = 50
    scale = 0.05 * len(nodes)
    scale = 50
    # print "Mean " + str(mean)
    # print "Scale " + str(scale)
    # print "Number of nodes " + str(len(nodes))
    for i in xrange(NUM_ROUNDS):
        rand_nodes = []
        while len(rand_nodes) < num_seeds:
            index = np.random.lognormal(mean, scale)
            index = int(index)
            if index >= 0 and index < len(nodes):
                if nodes[index] not in rand_nodes:
                    rand_nodes.append(nodes[index])
                    # print "Chose node with index {0}".format(index)
        for j in xrange(num_seeds):
            print rand_nodes[j]
