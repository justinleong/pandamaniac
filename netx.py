from utils.run import NUM_ROUNDS
from zero_player import get_graph_info
import networkx as nx

def do_networkx(f): 
    """Applies our framework for a given networkx metric f."""
    graph, num_players, num_seeds = get_graph_info()
    G = nx.from_dict_of_lists(graph)
    central_dict = f(G)
    sorted_list = sorted(central_dict, key=central_dict.get, reverse=True) 

    for i in xrange(NUM_ROUNDS):
        for j in xrange(num_seeds):
            print sorted_list[j] #, central_dict[sorted_list[j]]

def list_networkx(f):
    """Returns list of sorted nodes calculated with networkx metric f."""
    graph, num_players, num_seeds = get_graph_info()
    G = nx.from_dict_of_lists(graph)
    central_dict = f(G)
    sorted_list = sorted(central_dict, key=central_dict.get, reverse=True) 
    return sorted_list