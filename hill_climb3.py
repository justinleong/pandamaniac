from utils import sim
from utils.run import NUM_ROUNDS
from zero_player import get_graph_info
import networkx as nx

"""
v3.0 - trains on opponents (top12 or files run/ directory)
Position HILL at the end of the player list in run.py for best results
Takes graph and integer k as input; returns list of k best seed nodes
"""
MAX_GEN = 2

def run(graph, k, opponents=[]):
	A = []

	# sort nodes by high eigenvector centrality
	G = nx.from_dict_of_lists(graph)
	central_dict = nx.eigenvector_centrality(G)
	sorted_list = sorted(central_dict, key=central_dict.get, reverse=True)
	
	cutoff = min(len(sorted_list), k * 20) # consider 20 times needed seeds
	nodes = sorted_list[:cutoff] # get top nodes
	nodes = sorted_list

	# generate or load opponents
	opponents = get_opponents(k)

	# top12 based on degree
	top12dict = central_dict = nx.degree_centrality(G)
	top12list = sorted(top12dict, key=top12dict.get, reverse=True)
	top12 = top12list[:12]

	while len(A) < k and len(nodes) > 0:
		# find node x that maximizes marginal gain
		max_marginal_gain = 0
		max_x = nodes[0]
		
		for x in nodes: # for each node
			# node_mappings = opponents
			node_mappings = {"top12" : top12}
			node_mappings["bedlamites"] = A + [x]
			# node_mappings = {"bedlamites" : A + [x]}
			results = sim.run_simulation(graph, node_mappings, max_gen=MAX_GEN)
			marginal_gain = results["bedlamites"]
			if marginal_gain > max_marginal_gain:
				max_marginal_gain = marginal_gain
				max_x = x
		# found best x
		A.append(max_x) # where x maximizes marginal gain
		nodes.remove(max_x) # faster if remove by index?
	return A

"""
Grab our own strategies out of the run/ folder
"""
def get_opponents(num_seeds):
	opponents = {}
	for i in xrange(10):
		try:
			with open("run/{}.run".format(i)) as f:
				opponents[str(i) + ".run"] = []
				for line in f.readlines()[:num_seeds]:
					opponents[str(i) + ".run"].append(str(line.strip()))
		except IOError:
			break
	return opponents

if __name__ == "__main__":
	# print get_opponents([1, 2, 3])
    graph, num_players, num_seeds = get_graph_info()
    seeds = run(graph, num_seeds)

    for i in xrange(NUM_ROUNDS):
        for s in seeds:
            print s
