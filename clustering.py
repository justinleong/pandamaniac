from netx import do_networkx
from netx import list_networkx
import networkx as nx

def f():
	""" Returns list of top nodes sorted by eigenvector"""
	return list_networkx(nx.clustering)

if __name__ == "__main__":
    do_networkx(nx.clustering)
