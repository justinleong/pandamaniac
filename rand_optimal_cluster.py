from zero_player import get_graph_info
from utils.run import NUM_ROUNDS
from highest_degree import f
import networkx as nx
#from eigenvector import f
import numpy as np


if __name__ == "__main__":
    data, num_players, num_seeds = get_graph_info()
    nodes = f(data, num_players, num_seeds)
    G = nx.from_dict_of_lists(data)
    mean = num_players*num_seeds/2
    scale = num_players*num_seeds/6 
    # print "Mean " + str(mean)
    # print "Scale " + str(scale)
    # print "Number of nodes " + str(len(nodes))
    for i in xrange(NUM_ROUNDS):
        rand_nodes = []
        while len(rand_nodes) < 1:
            index = np.random.normal(num_seeds, scale)
            index = int(index)
            if index >= 5 and index < num_seeds*2:
                rand_nodes.append(nodes[index])
                king = nodes[index]
        while len(rand_nodes) < num_seeds:
            index = np.random.normal(mean, scale)
            index = int(index)
            if index >= 5 and index < num_players*num_seeds:
                if nodes[index] not in rand_nodes and nx.shortest_path_length(G, king, nodes[index]) < 3:
                    rand_nodes.append(nodes[index])
                    # print "Chose node with index {0}".format(index)
        for j in xrange(num_seeds):
            print rand_nodes[j]
