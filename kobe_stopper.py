from utils.run import NUM_ROUNDS
from zero_player import get_graph_info
import networkx as nx

# Highest degree nodes minus the top 10

def prune_graph(graph, num_players, num_seeds):
    G = nx.from_dict_of_lists(graph)
    central_dict = nx.closeness_centrality(G)
    sorted_list = sorted(central_dict, key=central_dict.get, reverse=True)
    remove_dict = nx.degree_centrality(G)
    remove_list = sorted(remove_dict, key=remove_dict.get, reverse=True)
    for i in xrange(5):
        sorted_list.remove(remove_list[i + 5])
        G.remove_node(remove_list[i + 5])
    return G

if __name__ == "__main__":
    graph, num_players, num_seeds = get_graph_info()
    G = nx.from_dict_of_lists(graph)
    central_dict = nx.degree_centrality(G)
    sorted_list = sorted(central_dict, key=central_dict.get, reverse=True)
    remove_dict = nx.degree_centrality(G)
    remove_list = sorted(remove_dict, key=remove_dict.get, reverse=True)
    for i in xrange(10):
        sorted_list.remove(remove_list[i])
    """remove_dict = nx.eigenvector_centrality(G)
    remove_list = sorted(remove_dict, key=remove_dict.get, reverse=True)
    for i in xrange(10):
        if remove_list[i] in sorted_list:
            sorted_list.remove(remove_list[i])"""

    for i in xrange(NUM_ROUNDS):
        for j in xrange(num_seeds):
            print sorted_list[j] #, central_dict[sorted_list[j]]
