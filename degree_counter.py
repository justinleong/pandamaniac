from utils.run import NUM_ROUNDS
from zero_player import get_graph_info
import networkx as nx

if __name__ == "__main__":
    graph, num_players, num_seeds = get_graph_info()
    G = nx.from_dict_of_lists(graph)
    deg = nx.degree_centrality(G)
    nodes = ['296', '304', '1113', '1145', '292', '1627', '284', '1163', '1130', '1142', '262', '261', '275', '260', '294', '1110', '1119', '1125', '1105', '294', '1110', '302', '285', '1109', '1119', '2888', '4057', '4832', '4315', '2534', '4648', '4650', '3416', '3416', '3413', '2887', '2360', '1635']
    kobe = {}
    for n in nodes:
        kobe[n] = deg[n]
    sorted_list = sorted(kobe, key = kobe.get, reverse=True)
    for i in xrange(NUM_ROUNDS):
        for j in xrange(num_seeds):
            print sorted_list[j]
