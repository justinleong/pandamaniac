import sys
import os
import json
from zero_player import get_graph_info
from utils.run import NUM_ROUNDS
from highest_degree import f
# from eigenvector import f

if __name__ == "__main__":
    data, num_players, num_seeds = get_graph_info()
    nodes = f(data, num_players, num_seeds)
    while True:
        input_nodes = raw_input("Enter nodes: ")
        try:
            input_nodes = input_nodes.split(" ")
            output_str = "Index: "
            for node in input_nodes:
                output_str += str(nodes.index(node)) + " "
            print output_str
        except:
            print "Invalid node\n"
