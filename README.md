# MidnightBedlamites Pandemaniac Code #

How do I play?
------
1. Save the graph into the Graphs/ directory with the appropriate name (e.g, 4.5.1.json)
2. If the game has two players, run the two-player simulation to determine the best strategy for the graph. Otherwise, if the game has four or more players, run the multi-player simulation.
3. Take the winning strategy and corresponding seed nodes as a suggestion; discuss with your teammates and adjust parameters before submitting.

How do I run the code?
------
### Run single strategy ###
* In the `pandamaniac` directory, run `python highest_degree.py 4.5.1.json`, replacing `highest_degree.py` with any strategy and `4.5.1.json` with any graph name. 
* The output is formatted for Pandemaniac, so pipe it into a file: `python highest_degree.py 4.5.1.json > output.txt`.
* Note that the commandline argument is the name of a graph that exists (e.g, as Graphs/4.5.1.json), NOT the path to the file.

### Run two-player simulation ###
* We recommend the default settings, but feel free to modify `NUM_ROUNDS` and `NUM_LEGIT_PLAYERS` or to add different strategies in `utils/two_player_run.py`.
* In the `pandamaniac` directory, run `python utils/two_player_run.py -f 4.5.1.json`, replacing `4.5.1.json` with any graph name.
* An abundance of useful information will be printed on screen, but take a look at the third to last line, which is a dictionary that gives the rounds won by each strategy. Run the winning strategy as a single strategy.

### Run multi-player simulation ###
* We recommend the default settings, but feel free to modify `NUM_ROUNDS` or to add different strategies to the `players` list on Line 119. If there are more strategies in `players` than necessary for the given game, the code will take the first N, where N is in the "N.x.y.json" argument.
* In the `pandamaniac` directory, run `python utils/run.py -f 4.5.1.json`, replacing `4.5.1.json` with any graph name.
* The winning strategy's seed nodes will be printed in `run/winner.run`
* The winning seed nodes for each round will be printed in `run/conglomerate_winners.run`

### Run graph visualization ###
* Run `python draw_graph.py 4.5.1.json`, replacing `4.5.1.json` with any graph name.

### Run simulation of downloaded results ###
* Run `python utils/run.py -f 4.10.1.json -d downloads/4.10.1-MidnightBedlamites.json`, replacing `4.10.1.json` with a graph name and `downloads/4.10.1-MidnightBedlamites.json` with a path to the corresponding downloaded JSON results file.

Other questions?
------
* Email us!