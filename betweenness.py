from netx import do_networkx
from netx import list_networkx
import networkx as nx

def f():
	""" Returns list of top nodes sorted by betweenness"""
	return list_networkx(nx.betweenness_centrality)

if __name__ == "__main__": 
    do_networkx(nx.betweenness_centrality)
