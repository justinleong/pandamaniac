from zero_player import get_graph_info
from utils.run import NUM_ROUNDS
#from highest_degree import f
from eigenvector import f
import numpy as np

# Choose a cluster of nodes

if __name__ == "__main__":
    data, num_players, num_seeds = get_graph_info()
    nodes = f(data, num_players, num_seeds)
    seed_nodes = []
    king = nodes[0] # assume this is center of largest cluster
    for node in nodes[1:]:
        # check if king is neighbor
        if king not in data[node]:
            seed_nodes.append(node)
            # check if we have enough seed nodes
            if len(seed_nodes) > num_seeds:
                break
    for i in xrange(NUM_ROUNDS):
        for j in xrange(num_seeds):
            print seed_nodes[j]
