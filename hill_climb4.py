from utils import sim
from utils.run import NUM_ROUNDS
from zero_player import get_graph_info
import eigenvector
import closeness

"""
v4.0 - do not include nodes that appear in both eigenvector and closeness
Takes graph and integer k as input; returns list of k best seed nodes
"""
def run(graph, num_players, k):
	# get nodes that appear in top k nodes of two measures of centrality
	overly_popular = []
	n1 = eigenvector.f(graph, num_players, k)[:k]
	n2 = closeness.f(graph, num_players, k)[:k]
	overly_popular = list(set(n1) & set(n2))

	A = []
	nodes = graph.keys()
	while len(A) < k and len(nodes) > 0:
		# in each round...
		# find node x that maximizes marginal gain
		max_marginal_gain = 0
		max_x = nodes[0]
		for x in nodes: # for each node
			if x not in overly_popular:
				node_mappings = {"bedlamites" : A + [x]}
				results = sim.run_simulation(graph, node_mappings, max_gen=2) # instead of 2, maybe a function of graph size
				marginal_gain = results["bedlamites"]
				if marginal_gain > max_marginal_gain:
					max_marginal_gain = marginal_gain
					max_x = x
		# found best x
		A.append(max_x) # where x maximizes marginal gain
		nodes.remove(max_x) # faster if remove by index?
	return A

if __name__ == "__main__":
    graph, num_players, num_seeds = get_graph_info()
    seeds = run(graph, num_players, num_seeds)
    for i in xrange(NUM_ROUNDS):
        for s in seeds:
            print s
