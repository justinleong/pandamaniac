import argparse
import json
from utils.run import NUM_ROUNDS

# A player that returns no nodes

def get_graph_info():
    """Parses graph from argument and """
    parser = argparse.ArgumentParser(description="Get graph.")
    parser.add_argument("graph", type=str)

    args = parser.parse_args()
    graph_file = open("Graphs/{0}".format(args.graph))
    graph = json.loads(graph_file.read())
    graph_file.close()

    num_players, num_seeds, gid, ext = args.graph.split(".")

    return graph, int(num_players), int(num_seeds)


if __name__ == "__main__":
    graph, num_players, num_seeds = get_graph_info()
