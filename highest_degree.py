import json
from zero_player import get_graph_info
from utils.run import NUM_ROUNDS

def f(data, num_players, num_seeds):
    # Inditialize degree counts to zero
    degree_count = {}
    for node in data:
        degree_count[node] = 0
    # Count degree of each node
    for node in data:
        for neighbor in data[node]:
            degree_count[neighbor] += 1
    
    # Print in order by degree
    sorted_list = sorted(degree_count, key=degree_count.get, reverse=True)
    return sorted_list

if __name__ == "__main__":
    data, num_players, num_seeds = get_graph_info()

    # Inditialize degree counts to zero
    degree_count = {}
    for node in data:
        degree_count[node] = 0
    # Count degree of each node
    for node in data:
        for neighbor in data[node]:
            degree_count[neighbor] += 1
    
    # Print in order by degree
    sorted_list = sorted(degree_count, key=degree_count.get, reverse=True)

    for i in xrange(NUM_ROUNDS):
        for j in xrange(num_seeds):
            print sorted_list[j] #, degree_count[sorted_list[j]]
