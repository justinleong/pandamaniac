from zero_player import get_graph_info
from utils.run import NUM_ROUNDS
from highest_degree import f
#from eigenvector import f
import numpy as np
import networkx as nx

# Choose nodes based on a normal distribution

if __name__ == "__main__":
    data, num_players, num_seeds = get_graph_info()
    nodes = f(data, num_players, num_seeds)
    G = nx.from_dict_of_lists(data)
    if num_players == 27:
        num_players = 16
    mean = num_players*num_seeds/4
    scale = num_players*num_seeds/12
    for i in xrange(NUM_ROUNDS):
        rand_nodes = []
        num_remove = 30 
        while len(rand_nodes) < num_seeds:
            index = np.random.normal(mean, scale)
            index = int(index)
            # Upper and lower cutoffs for nodes based on rank
            if index >= num_remove and index < num_players*num_seeds:
                if nodes[index] not in rand_nodes:
                    rand_nodes.append(nodes[index])
                    # print "Chose node with index {0}".format(index)
        for j in xrange(num_seeds):
            print rand_nodes[j]
