from utils import sim
from utils.run import NUM_ROUNDS
from zero_player import get_graph_info
import networkx as nx

"""
v2.0 - considers fewer candidate nodes
Takes graph and integer k as input; returns list of k best seed nodes
"""
def run(graph, k):
	A = []
	#nodes = graph.keys()
	# sort nodes by high eigenvector centrality
	G = nx.from_dict_of_lists(graph)
	central_dict = nx.eigenvector_centrality(G)
	sorted_list = sorted(central_dict, key=central_dict.get, reverse=True)
	
	cutoff = min(len(sorted_list), k * 20) # consider 20 times needed seeds
	nodes = sorted_list[:cutoff] # get top nodes

	influence = 0
	while len(A) < k and len(nodes) > 0:
		# in each round...
		# find node x that maximizes marginal gain
		max_marginal_gain = 0
		max_x = nodes[0]
		for x in nodes: # for each node
			node_mappings = {"bedlamites" : A + [x]}
			results = sim.run_simulation(graph, node_mappings, max_gen=2) # instead of 2, maybe a function of graph size
			marginal_gain = results["bedlamites"]
			if marginal_gain > max_marginal_gain:
				max_marginal_gain = marginal_gain
				max_x = x
		# found best x
		A.append(max_x) # where x maximizes marginal gain
		nodes.remove(max_x) # faster if remove by index?
	return A

if __name__ == "__main__":
    graph, num_players, num_seeds = get_graph_info()
    seeds = run(graph, num_seeds)
    for i in xrange(NUM_ROUNDS):
        for s in seeds:
            print s
