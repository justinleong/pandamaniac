import argparse
import os
import json
from zero_player import get_graph_info
from utils import sim
import numpy as np
import matplotlib.pyplot as plt

NUM_ROUNDS = 50

ALGORITHM = "DEGREE"
DRAW = False 

if ALGORITHM == 'DEGREE':
    from highest_degree import f
elif ALGORITHM == 'EIGEN':
    from eigenvector import g as f

def get_index(node, nodes):
    try:
        return nodes.index(node)
    except:
        return

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run simulation on a given file ")
    parser.add_argument("-f", help="graph (ex: 8.20.1.json)") 
    parser.add_argument("-d", help="downloaded results file") 


    args = parser.parse_args()

    if args.f != None:
        graph_file = open("Graphs/{0}".format(args.f))
        graph = json.loads(graph_file.read())
        graph_file.close()
        num_players, num_seeds, gid, ext = args.f.split(".")
        num_players = int(num_players)
        num_seeds = int(num_seeds)
        nodes = f(graph, num_players, num_seeds)
    # Set up NUM_ROUNDS of competition.
    game_rounds = []
    for i in xrange(NUM_ROUNDS): 
        game_rounds.append({})

    # RUN SIM OPTION 1: use file downloaded from website
    if args.d != None:
        download = args.d

        num_players, num_seeds, gid, ext = args.f.split(".")
        num_players = int(num_players)
        num_seeds = int(num_seeds)

        # Load seed nodes for each round into game_rounds
        node_indices = np.arange(len(nodes))
        with open(download, 'r') as f:
            data = f.readlines() # one line
            # BE CAREFUL ABOUT WHAT YOU FEED TO EVAL()
            team_strategies = eval(data[0])


            for i in xrange(NUM_ROUNDS):
                team_num = 0
                for team in team_strategies:
                    game_rounds[i]["{0} {1}d".format(team, team_num)] = team_strategies[team][i][:num_seeds]
                    team_num += 1
            total_teams = team_num

            # Write team_num.run files; 50 lines per file
            team_num = 0
            for team in team_strategies:
                print team
                indices = []
                with open("run/{}.mama".format(team_num), 'w') as f:
                    for rnd in team_strategies[team]:
                        rank = 0
                        rnd_indices = []
                        for seed_node in rnd:
                            if rank == num_seeds:
                                rank = 0
                                break
                            try:
                                index = get_index(seed_node, nodes)
                                rnd_indices.append(index)
                            except:
                                pass
                            rank += 1
                        # print rnd_indices
                        indices.extend(rnd_indices)
                team_num += 1
                counts = []
                node_indices = np.arange(max(indices))
                node_indices_ticks = np.arange(0, max(indices), float(max(indices)) / 10.0)
                for i in node_indices:
                    counts.append(indices.count(i))
                f = plt.figure()
                plt.suptitle(team + " " + ALGORITHM)
                ax = plt.axes()
                ax.set_xticks(node_indices_ticks + 1.0 / 2)
                ax.set_xticklabels(node_indices_ticks)
                plt.bar(node_indices, counts, 1.0, color='r')
                plt.savefig(team + "_" + ALGORITHM + "_" + str(num_players) + "." + str(num_seeds) + "." + str(gid) + ".jpg")
                if DRAW:
                    plt.show()
                print "----------------------------------"

