import networkx as nx
import matplotlib.pyplot as plt
from zero_player import get_graph_info

# Tool for visualizing graphs
if __name__ == "__main__":
    graph, num_players, num_seeds = get_graph_info()
    G = nx.from_dict_of_lists(graph)
    nx.draw(G, node_size=10, alpha=0.1, font_color='purple', with_labels = True, style='dashed')
    plt.show()
